/** @jsx jsx */
import {  css } from "@emotion/core";
const widthPx = 40;
const heightPx = 40;
const redbkg = "red";
const borderRadius = 2;
const blackbkg = "black";
export const board = css`
  .board {
    background-color: antiquewhite;
  }
  .TDB {
    background-color: black;
  }
  .TDW {
    background-color: white;
  }
  p{
    margin: auto;
  }
  td {
    width: ${widthPx}px;
    height: ${heightPx}px;
    > p {
      width: ${widthPx - borderRadius * 2}px;
      height: ${heightPx - borderRadius * 2}px;     
      border-color: white;
      border-style: solid;
      border-width: ${borderRadius}px;
      border-radius: 50%;
    }
  }
  table {
    width: ${widthPx * 8}px;
    height: ${heightPx * 8}px;
  }
  .red {
    background-color: ${redbkg};
  }
  .Black {
    background-color: ${blackbkg};
  }
  .ScoreBoard{
    flex-direction: row;
    display: flex;
    justify-items: center;
    justify-content: space-between;
  }
  .changeTurn{
    background-color: antiquewhite;
    border-style: solid;
    border-color: red;
    color: red;
  }
`;
/** @jsx jsx */
// eslint-disable-next-line
import React, { getGlobal,setGlobal, useGlobal,useEffect } from "reactn";
import { checkMove, getTheScores } from "../Code/CheckersFunction";
import { board } from "./Style";
import { jsx } from "@emotion/core";

export const Board = () => {
  const global = getGlobal();
  window.global = global;
  const [Selected, setSelected] = useGlobal("Selected");
  const [turn,setTurn]=useGlobal('turn')
  const scores = global.scores;
  const boardMap = global.boardMap;
//   let turn = global.turn;
  let  total = global.total;  
  const renderPiece = (i)=>{
      let RP=null;
      switch (i) {
          case 1:RP=<p className="red" />;break;
          case 2:RP=<p className="Black" />;break;
          case 3:RP=<p className="red" >K</p>;break;
          case 4:RP=<p className="Black" >K</p>;break;      
          default:RP=<></>
              break;
      }
      return RP;
  }
  useEffect(() => {  
      let [scores,total] = getTheScores(boardMap);
    setGlobal({scores,total})    
  }, [boardMap]);
      
  
  return (
    <div css={board}>
      <table className="board">
        <tbody>
          {boardMap.map((item, i) => (
            <tr key={"id_" + i}>
              {item.map((item, j) => (
                <td
                  className={(i + j) % 2 === 0 ? "TDW" : "TDB"}
                  key={"id_" + i + j}
                  onClick={() => {
                    checkMove(i,j,turn)
                    setSelected([i, j]);
                  }}
                >{
                renderPiece(item)
                }                
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      
      <div className="ScoreBoard">
      <p>
        this is Selected : {Selected[0]},{Selected[1]}
      </p>
      <span className='changeTurn' onClick={()=>{          
          setTurn(turn==='Black'?'Red':'Black')          
      }}>changeTurn</span>

      </div>
      <div className="ScoreBoard">
        <p>Black : {scores[0]}</p>
        <div>
        <p>turn : {turn}</p>
        <p>total : {total}</p>

        </div>
        <p>red : {scores[1]}</p>
      </div>
    </div>
  );
};
export default Board;

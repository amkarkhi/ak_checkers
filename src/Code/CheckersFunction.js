import { getGlobal, setGlobal, useEffect } from "reactn";
const movePiece = (old, curr) => {
  let global = getGlobal();
  let boardMap = global.boardMap;
  let [oi, oj] = old;
  let [ni, nj] = curr;
  let tmp = boardMap[oi][oj];
  boardMap[oi][oj] = boardMap[ni][nj];
  boardMap[ni][nj] = tmp;
  if (boardMap[ni][nj] === 2 && ni === 0) boardMap[ni][nj] = 4;
  else if (boardMap[ni][nj] === 1 && ni === 7) boardMap[ni][nj] = 3;
  setGlobal({ boardMap });
};
export const checkMove = (i, j) => {
  let global = getGlobal();
  let boardMap = global.boardMap;
  let turn = global.turn;
  console.log(i, j, turn, boardMap[i][j]);
  let availableMoves = [[], []];
  if (boardMap[i][j] === 0) {
    console.log("nothing");
    availableMoves = global.availableMoves;
    let prvMove = global.prvMove;
    if (availableMoves[0].includes(i) && availableMoves[1].includes(j))
      movePiece(prvMove, [i, j]);
    return;
  }
  //selecting a new piece
  let prvMove = [i, j];
  if (turn === "Red" && boardMap[i][j] === 1) {
    availableMoves[0] = [i + 1];
    availableMoves[1] = [j - 1, j + 1];
  } else if (turn === "Black" && boardMap[i][j] === 2) {
    availableMoves[0] = [i - 1];
    availableMoves[1] = [j - 1, j + 1];
  } else if (turn === "Red" && boardMap[i][j] === 3) {      
      availableMoves[0] = [i + 1, i - 1];
      availableMoves[1] = [j - 1, j + 1];
    } else if (turn === "Black" && boardMap[i][j] === 4) {      
    availableMoves[0] = [i - 1, i + 1];
    availableMoves[1] = [j - 1, j + 1];
  }
  availableMoves = availableMoves.map(item =>
    item.filter(t => t >= 0 && t < 8)
  );
  setGlobal({ availableMoves, prvMove });
  console.log(availableMoves);
};
export const getTheScores = Boardmap => {
  const Weight = getGlobal().Weight;
  let scores = [0, 0];
  let total = 0;
  for (let i in Boardmap) {
    for (let j in Boardmap[i]) {
      total += Weight[Boardmap[i][j]];
      switch (Boardmap[i][j]) {
        case 1:
          scores[0]++;
          break;
        case 3:
          scores[0] += 2;
          break;
        case 2:
          scores[1]++;
          break;
        case 4:
          scores[1] += 2;
          break;
        default:
          break;
      }
    }
  }
  return [scores, total];
};
